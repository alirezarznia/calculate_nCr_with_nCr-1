#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;

using namespace std;
ll dp[1001][1001] ,cnt;
ll cont(ll n , ll d)
{
    if(n==0 || abs(d) >n)
        return 0;
    if(dp[n][d] != -1)
        return dp[n][d];
    if(n==1 && d == 0)
        return 2;
    if(n==1 && abs(d) == 1)
        return 1;
    return dp[n][d] = (2 * cont(n-1 , d)+ cont(n-1 , d-1) + cont(n-1 , d+1))%cnt;
}
int main()
{
  //  Test;
    ll n  ,t ,m ;
    cin>>t;
    cnt = pow(10 ,9)+7;
    while(t--)
    {
        Set(dp ,-1);
        cin>>n;
        cout<<cont(n ,0)<<endl;
    }

}