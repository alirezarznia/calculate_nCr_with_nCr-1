int countSeq(int n)
{
    int nCr=1, res = 1;
 
    // Calculate SUM ((nCr)^2)
    for (int r = 1; r<=n ; r++)
    {
        // Compute nCr using nC(r-1)
        // nCr/nC(r-1) = (n+1-r)/r;
        nCr = (nCr * (n+1-r))/r;   
 
        res += nCr*nCr;
    }
 
    return res;
}
 
// Driver program
int main()
{
    int n = 3;
    cout << "Count of sequences is "
         << countSeq(n);
    return 0;
}